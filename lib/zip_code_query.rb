require "zip_code_query/railtie"
require "zip_code_query/response"
require "zip_code_query/search_by_address_response"
require "zip_code_query/providers/via_cep"
require "zip_code_query/providers/republica_virtual"

module ZipCodeQuery

  class << self

    attr_accessor :zip_code_provider

  end

  self.zip_code_provider = 'ViaCep'

  def self.configure
    yield(self)
  end

  class Query

    # Necessário para poder ser serializado com ActiveModelSerializer
    include ActiveModel::Serialization

    attr_accessor :uf, :cidade, :bairro, :logradouro, :zipcode, :estado, :took

    def initialize(zipcode)
      # Sempre tem que ser passado uma string, e nunca integer, sob pena de um número começado com 0 retornar Invalid octal digit
      # Removemos espaços em brancos (strip) e todos os caracteres não numéricos
      zipcode = zipcode.strip.scan(/\d/).join('') if zipcode.instance_of?(String)
      @zipcode = zipcode
    end

    def perform

      return false if self.zipcode.blank?

      time = Time.now

      response = "ZipCodeQuery::Providers::#{ZipCodeQuery.zip_code_provider}".constantize.perform(zipcode)

      self.took = Time.now - time

      if response.success?

        self.uf         = response.uf
        self.estado     = response.estado
        self.cidade     = response.cidade
        self.bairro     = response.bairro
        self.logradouro = response.logradouro
        return true
      else
        return false
      end

    end

  end
end
