module ZipCodeQuery

  class Response

    STATES = {
      'AC': 'Acre',
      'AL': 'Alagoas',
      'AP': 'Amapá',
      'AM': 'Amazonas',
      'BA': 'Bahia',
      'CE': 'Ceará',
      'DF': 'Distrito Federal',
      'ES': 'Espírito Santo',
      'GO': 'Goiás',
      'MA': 'Maranhão',
      'MT': 'Mato Grosso',
      'MS': 'Mato Grosso do Sul',
      'MG': 'Minas Gerais',
      'PA': 'Pará',
      'PB': 'Paraíba',
      'PR': 'Paraná',
      'PE': 'Pernambuco',
      'PI': 'Piauí',
      'RJ': 'Rio de Janeiro',
      'RN': 'Rio Grande do Norte',
      'RS': 'Rio Grande do Sul',
      'RO': 'Rondônia',
      'RR': 'Roraima',
      'SC': 'Santa Catarina',
      'SP': 'São Paulo',
      'SE': 'Sergipe',
      'TO': 'Tocantins',
    }

    include ActiveModel::Model

    attr_accessor :success

    ATTRIBUTES = [:uf, :cidade, :logradouro, :bairro]

    attr_reader *ATTRIBUTES

    ATTRIBUTES.each do |attribute|

      define_method("#{attribute}=") do |val|

        instance_variable_set("@#{attribute}", sanitize(val))

      end

    end

    def success?
      @success
    end

    def estado
      uf = self.uf.try(:upcase).try(:to_sym)
      if uf
        STATES[self.uf.upcase.to_sym]
      else
        nil
      end
    end

    private

      def sanitize(val)
        ActionController::Base.helpers.sanitize(val)
      end

  end

end