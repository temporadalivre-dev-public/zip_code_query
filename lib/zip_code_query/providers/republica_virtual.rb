require 'httparty'
require 'cgi'

module ZipCodeQuery

  module Providers

    module RepublicaVirtual

      def self.perform(zipcode)

        response = HTTParty.get("http://cep.republicavirtual.com.br/web_cep.php?cep=#{zipcode}&formato=query_string")

        response = CGI::parse(response.body)

        resultado = response['resultado'].first.to_i

        if resultado == 1 || resultado == 2

          ZipCodeQuery::Response.new(
            success: true,
            uf: sanitize(response["uf"][0].to_s),
            cidade: sanitize(response["cidade"].first),
            bairro: sanitize(response["bairro"].first),
            logradouro: sanitize(response["tipo_logradouro"].first + " " + response["logradouro"].first),
          )

        else

          ZipCodeQuery::Response.new(success: false)

        end

      end

      def self.sanitize(param)
        # Convertemos de "ISO-8859-1" para UTF8 para evitar "S\xE3o Paulo" ou "Maring\xE1"
        # Nos meus testes a origem também pode ser considerada "Windows-1252"
        # O .encode abaixo usa os argumentos encoding_DESTINO, encoding_ORIGEM
        # Fonte: http://www.justinweiss.com/articles/3-steps-to-fix-encoding-problems-in-ruby/
        param = param.encode("UTF-8","ISO-8859-1")

      end

    end

  end


end