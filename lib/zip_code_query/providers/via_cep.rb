require 'httparty'

module ZipCodeQuery

  module Providers

    module ViaCep

      ENCODING_OPTIONS = {
        :invalid           => :replace,  # Replace invalid byte sequences
        :undef             => :replace,  # Replace anything not defined in ASCII
        :replace           => '',        # Use a blank for those replacements
      }

      def self.sanitize_string_for_url(string)

        string = I18n.transliterate(string.gsub(",", ""))

        # Fonte: https://stackoverflow.com/a/9420531/1290457
        string = string.squish.encode(Encoding.find('ASCII'), **ENCODING_OPTIONS)

        ERB::Util.url_encode(string)

      end

      def self.perform_cep_search_by_address(uf: , city: , logradouro: )

        url = "https://viacep.com.br/ws/#{sanitize_string_for_url(uf)}/#{sanitize_string_for_url(city)}/#{sanitize_string_for_url(logradouro)}/json/"

        p url

        response = HTTParty.get(url)

        if response.success?
          ZipCodeQuery::SearchByAddressResponse.new(success: true, results: response)
        else
          ZipCodeQuery::SearchByAddressResponse.new(success: false)
        end

      end

      def self.perform(zipcode)

        response = HTTParty.get("https://viacep.com.br/ws/#{zipcode}/json/")

        if response.success?

          ZipCodeQuery::Response.new(
            success: true,
            uf: response["uf"],
            cidade: response["localidade"],
            bairro: response["bairro"],
            logradouro: response["logradouro"],
          )

        else

          ZipCodeQuery::Response.new(success: false)

        end

      end

    end

  end


end
