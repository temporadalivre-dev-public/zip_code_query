require 'httparty'

module ZipCodeQuery

  module ViaCep

    def self.perform(zipcode)

      response = HTTParty.get("https://viacep.com.br/ws/#{zipcode}/json/")

      if response.success?

        ZipCodeQuery::Response.new(
          success: true,
          uf: response["uf"],
          cidade: response["localidade"],
          logradouro: response["logradouro"],
          bairro: response["bairro"],
        )

      else

        ZipCodeQuery::Response.new(success: false)

      end

    end

  end

end