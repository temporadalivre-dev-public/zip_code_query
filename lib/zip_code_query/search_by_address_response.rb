module ZipCodeQuery

  class SearchByAddressResponse

    include ActiveModel::Model

    attr_accessor :success, :results

    def success?
      @success
    end

  end

end