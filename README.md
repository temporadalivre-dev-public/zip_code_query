# ZipCodeQuery
Query de ZipCode (CEP) brasileiro.

## Usage
1. Adicione ao Gemfile
2. Suportamos query via ViaCep e RepublicaVirtual. O padrão é ViaCep (resultados mais atualizados nos nossos testes), mas a resposta da API é cerca de 600ms, contra 110ms do RepublicaVirtual.
3. Se quiser mudar o padrão (ViaCep) defina em um initializer:

```
ZipCodeQuery.zip_code_provider = 'RepublicaVirtual'
```

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'zip_code_query'
```

And then execute:
```bash
$ bundle
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
